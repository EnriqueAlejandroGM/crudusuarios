const router = require('express').Router()
const Users = require('../models/users')
const controller = require('../controllers/users')

//VISTAS
router.get('/usuarios', (req, res) => {
    res.render('pages/usuario_lista')
})
router.get('/usuarios:nombre', (req, res) => {
    res.render('pages/usuario_ver')
})
router.post('/usuarios:nombre', (req, res) => {
    res.render('pages/usuario_crear')
})
router.put('/usuarios:id/:sustituto', (req, res) => {
    res.render('pages/usuario_actualizar')
})
router.delete('/usuarios:id', (req, res) => {
    res.render('pages/usuario_eliminar')
})


//API
router.get('/api/usuarios/', controller.readList)

router.get('/api/usuarios/:nombre', controller.readOne)

router.post('/api/usuarios', controller.createOne)

router.put('/api/usuarios/:id/:sustituto', controller.updateOne)

router.delete('/api/usuarios/:id', controller.deleteOne)

router.get('/myip/:ip', controller.showIp)

module.exports = router