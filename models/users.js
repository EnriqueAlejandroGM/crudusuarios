const mongoose = require('mongoose')
 
var schema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
    },
    apellido: {
        type: String,
        required: true,
    },
    edad: {
        type: Number,
        required: true,
    },
    casado: {
        type: Boolean,
        required: true,
    }
})

var model = mongoose.model('User', schema)

module.exports = model