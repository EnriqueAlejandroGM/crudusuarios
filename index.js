const express = require('express')
const app = express()
const port = 3003
const mongoose = require('mongoose');
const mongoConfig = { useNewUrlParser: true }
const bodyParser = require('body-parser')
const morgan = require('morgan')
const helmet = require('helmet')
const cors = require('./middlewares/cors')
var geoip = require('geoip-lite');

//Base de datos
mongoose.connect('mongodb://localhost:27017/test', mongoConfig);

//Configuración de express
app.use(bodyParser.urlencoded({ extend: true }))
app.use(bodyParser.json())
app.set('views', './views')
app.set('view engine', 'pug')
morgan('tiny')
app.use(cors)
app.use(helmet())
app.use(express.static('public'))


//Rutas
app.get('/', function (req, res) {
    res.render('index', { title: 'Hey', message: 'Hello there!'});
  });
app.get('/listar', function (req, res) {
    res.render('pages/usuario_lista', { title: 'Listar usuarios', message: 'Hello there!'});
  });
app.get('/ver', function (req, res) {
    res.render('pages/usuario_ver', { title: 'Ver un usuario', message: 'Hello there!'});
  });
app.get('/crear', function (req, res) {
    res.render('pages/usuario_crear', { title: 'Crear un usuario', message: 'Hello there!'});
  });
app.get('/actualizar', function (req, res) {
    res.render('pages/usuario_actualizar', { title: 'Actualizar un usuario', message: 'Hello there!'});
  });
app.get('/eliminar', function (req, res) {
    res.render('pages/usuario_eliminar', { title: 'Eliminar un usuario', message: 'Hello there!'});
  });


app.get('/felicidades/:de/:para', (req, res) => {
    const remitente = req.params.de
    const receptor = req.params.para 
    res.send('Hola ' + receptor + '! muchas felicidades. De: ' + remitente)
})
app.get('/zoo/:animal/:sonido', (req, res) => {
    const sonido = req.params.sonido
    res.send('El ' + animal + ' hace ' + sonido)
})

const rutas = require('./routes/users')
app.use('/', rutas)



app.listen(port, () => console.log(`Example app listen on port ${port}!`))