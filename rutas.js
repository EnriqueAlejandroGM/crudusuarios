const router = require('express').Router()
const Users = require('./models/users')


router.get('/usuarios/', (req, res) =>{
    Users.find({})
      .then((data)=>{
        console.log(data);
        res.json(data)
    })
    .catch((err)=>{
        console.log(err);
    })
})
router.get('/usuarios/:nombre', (req, res) =>{
    const nombre = req.params.nombre
    Users.find({nombre})
 .then((doc)=>{
    console.log(doc);
    res.json(doc)
  })
 .catch((err)=>{
   console.log(err);
 })
})

router.post('/usuarios', (req, res) => {
    //req.body muestra lso parámetros que le mandas
    console.log(req.body)
    const usuarioNuevo = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        edad: req.body.edad,
        casado: req.body.casado
    }
    return new Users(usuarioNuevo)
        .save()
        .then((doc) => {
            console.log('Usuario grabado en la base de datos', doc)
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al guardar usuario en la DB'
            console.error(message, error)
            res.status(500).json({ message })
        })
})

router.put('/usuarios/:id/:sustituto', (req, res) => {
    const id = req.params.id
    Users.findByIdAndUpdate(id, { nombre: req.params.sustituto })
    .then((doc) => {
        console.log('Usuario ' + id + ' actualizado ')
    })
    .catch((error) => {
        console.log(error)
    })
})

router.delete('/usuarios/:nombre', (req, res) => {
    const borrar = req.params.nombre
    Users.findOneAndRemove(borrar)
    .then((doc) => {
        console.log('Usuario ' + borrar + ' Eliminado')
        res.send('Usuario ' + borrar + ' Eliminado')
    })
})

module.exports = router