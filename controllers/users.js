const router = require('express').Router()
const Users = require('../models/users')
var geoip = require('geoip-lite');

function readList (req, res) {
    Users.find({})
      .then((data)=>{
        console.log(data);
        res.json(data)
    })
    .catch((err)=>{
        console.log(err);
    })
}

function readOne (req, res) {
    const nombre = req.params.nombre
    Users.find({nombre})
 .then((doc)=>{
    console.log(doc);
    res.json(doc)
  })
 .catch((err)=>{
   console.log(err);
 })
}

function  createOne (req, res) {
    //req.body muestra lso parámetros que le mandas
    console.log(req.body)
    const usuarioNuevo = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        edad: req.body.edad,
        casado: req.body.casado
    }
    return new Users(usuarioNuevo)
        .save()
        .then((doc) => {
            console.log('Usuario grabado en la base de datos', doc)
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al guardar usuario en la DB'
            console.error(message, error)
            res.status(500).json({ message })
        })
}

function updateOne (req, res) {
    const id = req.params.id
    Users.findByIdAndUpdate(id, { nombre: req.params.sustituto })
    .then((doc) => {
        console.log('Usuario ' + id + ' actualizado ')
    })
    .catch((error) => {
        console.log(error)
    })
}

function deleteOne (req, res) {
    const borrar = req.params.id
    Users.findByIdAndDelete(borrar)
    .then((doc) => {
        console.log('Usuario ' + borrar + ' Eliminado')
        res.send('Usuario ' + borrar + ' Eliminado')
    })
}

function showIp (req, res) {
    var ip = req.params.ip;
    var geo = geoip.lookup(ip);
    res.json(geo);
}


module.exports = {
    readList,
    readOne,
    createOne,
    updateOne,
    deleteOne,
    showIp
}